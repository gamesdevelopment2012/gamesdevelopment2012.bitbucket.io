
var userAgent = navigator.userAgent || navigator.vendor || window.opera;

if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
{
	window.location = "download.html";
}
else if( userAgent.match( /Android/i ) )
{
	window.location = "app.apk";
}
else
{
	window.location = "desktop.html";
}